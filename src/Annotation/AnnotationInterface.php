<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 1:41
 */

namespace Arilas\Proxy\Annotation;


interface AnnotationInterface
{
    /**
     * @param $value
     * @return mixed
     */
    public function setValue($value);

    /**
     * @return string
     */
    public function getValue();

    /**
     * @return string
     */
    public function toString();
} 