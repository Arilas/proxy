<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 2:29
 */

namespace Arilas\Proxy\Annotation;


use Arilas\Proxy\Exception\AnnotationException;

class DoctrineAnnotation implements AnnotationInterface
{
    const FORMAT = '@%s(%s)';
    /** @var  string */
    protected $type;
    /** @var  string */
    protected $value = '';

    public function toString()
    {
        if (is_null($this->type)) {
            throw new AnnotationException(
                'Doctrine Annotation must have type and value'
            );
        }

        return sprintf(static::FORMAT, $this->type, $this->value);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}