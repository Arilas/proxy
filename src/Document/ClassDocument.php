<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 3:05
 */

namespace Arilas\Proxy\Document;


use Arilas\Proxy\Annotation\AnnotationInterface;
use Arilas\Proxy\Element\Method;
use Arilas\Proxy\Element\PhpDoc;
use Arilas\Proxy\Element\Property;

class ClassDocument implements DocumentInterface
{
    const BEGIN_FILE = '<?php';
    const NAMESPACE_LINE = 'namespace %s;';
    const USE_LINE = 'use %s;';
    const CONSTANT_LINE = 'const %s = %s;';
    const CLASS_LINE = 'class %s';
    const EXTENDS_CLAUSE = 'extends %s';
    const IMPLEMENT_CLAUSE = 'implements %s';
    const BEGIN_BODY = '{';
    const END_BODY = '}';

    /** @var  string */
    protected $namespace;

    protected $uses = [];

    protected $name;

    protected $extends;

    protected $implements = [];

    /** @var PhpDoc */
    protected $annotations;
    /** @var array */
    protected $constants = [];
    /** @var array */
    protected $traits = [];
    /** @var Property[] */
    protected $properties = [];
    /** @var Method[] */
    protected $methods = [];

    public function __construct()
    {
        $this->annotations = new PhpDoc();
    }

    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    public function addUseStatement($use, $alias = null)
    {
        $use = (is_null($alias))
            ? ($use)
            : ($use . ' as ' . $alias);

        $this->uses[] = sprintf(static::USE_LINE, $use);
    }

    public function addClassAnnotation(AnnotationInterface $annotation)
    {
        $this->annotations->addAnnotation($annotation);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setExtends($extends)
    {
        $this->extends = $extends;
    }

    public function addImplement($implemented)
    {
        $this->implements[] = $implemented;
    }

    public function addConstant($name, $value)
    {
        $this->constants[] = sprintf(static::CONSTANT_LINE, $name, $value);
    }

    public function addTrait($name)
    {
        $this->traits[] = sprintf(static::USE_LINE, $name);
    }

    public function addProperty(Property $property)
    {
        $this->properties[] = $property;
    }

    public function addMethod(Method $method)
    {
        $this->methods[] = $method;
    }

    public function toString()
    {
        $uses = join(PHP_EOL, $this->uses);
        if ($uses != '') {
            $uses .= PHP_EOL . PHP_EOL;
        }

        $constants = join(PHP_EOL, $this->constants);
        if ($constants != '') {
            $constants = $this->applyIndent($constants, 4) . PHP_EOL . PHP_EOL;
        }

        $traits = join(PHP_EOL, $this->traits);
        if ($traits != '') {
            $traits = $this->applyIndent($traits, 4) . PHP_EOL . PHP_EOL;
        }

        $text = static::BEGIN_FILE . PHP_EOL;
        $text .= PHP_EOL . sprintf(static::NAMESPACE_LINE, $this->namespace) . PHP_EOL;
        $text .= PHP_EOL . $uses;
        $text .= $this->annotations->toString();
        $text .= sprintf(static::CLASS_LINE, $this->name);
        if (!is_null($this->extends)) {
            $text .= ' ' . sprintf(static::EXTENDS_CLAUSE, $this->extends);
        }
        if (!empty($this->implements)) {
            $text .= ' ' . sprintf(static::IMPLEMENT_CLAUSE, join(', ', $this->implements));
        }
        $text .= PHP_EOL . static::BEGIN_BODY . PHP_EOL;
        $text .= $constants;
        $text .= $traits;

        foreach ($this->properties as $property) {
            $text .= $property->toString() . PHP_EOL . PHP_EOL;
        }

        foreach ($this->methods as $method) {
            $text .= $method->toString() . PHP_EOL . PHP_EOL;
        }

        $text .= static::END_BODY;

        return $text;
    }

    protected function applyIndent($template, $indent)
    {
        $parts = explode(PHP_EOL, $template);
        $parts = array_map(
            function ($value) use ($indent) {
                return str_repeat(' ', $indent) . $value;
            },
            $parts
        );
        return join(PHP_EOL, $parts);
    }
}