<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 1:38
 */

namespace Arilas\Proxy\Document;


use Arilas\Proxy\Annotation\AnnotationInterface;
use Arilas\Proxy\Element\Method;
use Arilas\Proxy\Element\Property;

interface DocumentInterface
{
    public function setNamespace($namespace);

    public function addUseStatement($use, $alias = null);

    public function addClassAnnotation(AnnotationInterface $annotation);

    public function setName($name);

    public function addProperty(Property $property);

    public function addMethod(Method $method);

    public function toString();
} 