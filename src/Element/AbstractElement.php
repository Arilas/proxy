<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 1:33
 */

namespace Arilas\Proxy\Element;


abstract class AbstractElement implements ElementInterface
{
    /** @var int */
    protected $indent = 0;

    /**
     * @return int
     */
    public function getIndent()
    {
        return $this->indent;
    }

    /**
     * @param int $indent
     */
    public function setIndent($indent)
    {
        $this->indent = $indent;
    }

    protected function applyIndent($template)
    {
        $parts = explode(PHP_EOL, $template);
        $parts = array_map(
            function ($value) {
                return str_repeat(' ', $this->indent) . $value;
            },
            $parts
        );
        return join(PHP_EOL, $parts);
    }
} 