<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 1:33
 */

namespace Arilas\Proxy\Element;


interface ElementInterface
{
    public function setIndent($indent);

    public function toString();
} 