<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 1:43
 */

namespace Arilas\Proxy\Element;


use Arilas\Proxy\Annotation\AnnotationInterface;
use Arilas\Proxy\Exception\ElementException;

class Property extends AbstractElement
{
    const ACCESS_PUBLIC = 'public';
    const ACCESS_PRIVATE = 'private';
    const ACCESS_PROTECTED = 'protected';

    const FORMAT = '%s %s;';
    const FORMAT_VALUE = '%s %s = %s;';

    /** @var  PhpDoc */
    protected $phpDoc;
    /** @var  string */
    protected $name;
    /** @var  string */
    protected $value;
    /** @var  string */
    protected $accessible = self::ACCESS_PROTECTED;
    /** @var int */
    protected $indent = 4;

    public function __construct()
    {
        $this->phpDoc = new PhpDoc();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = '$' . $name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getAccessible()
    {
        return $this->accessible;
    }

    /**
     * @param string $accessible
     * @throws ElementException
     */
    public function setAccessible($accessible)
    {
        if (!in_array(
            $accessible,
            [
                static::ACCESS_PUBLIC,
                static::ACCESS_PRIVATE,
                static::ACCESS_PROTECTED,
            ]
        )
        ) {
            throw new ElementException (
                'Accessible must be public, private or protected'
            );
        }
        $this->accessible = $accessible;
    }

    public function addAnnotation(AnnotationInterface $annotation)
    {
        $this->phpDoc->addAnnotation($annotation);
    }

    /**
     * @return string
     * @throws ElementException
     */
    public function toString()
    {
        if (is_null($this->name)) {
            throw new ElementException(
                'Property must have name'
            );
        }

        $property = $this->phpDoc->toString();
        if (is_null($this->value)) {
            $property .= sprintf(static::FORMAT, $this->accessible, $this->name);
        } else {
            $property .= sprintf(static::FORMAT_VALUE, $this->accessible, $this->name, $this->value);
        }

        return $this->applyIndent($property);
    }
}