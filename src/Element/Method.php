<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 2:44
 */

namespace Arilas\Proxy\Element;


use Arilas\Proxy\Annotation\AnnotationInterface;
use Arilas\Proxy\Exception\ElementException;

class Method extends AbstractElement
{
    const ACCESS_PUBLIC = 'public';
    const ACCESS_PRIVATE = 'private';
    const ACCESS_PROTECTED = 'protected';

    const BEGIN_LINE = "%s function %s(%s)";
    const BEGIN_BODY = "{";
    const END_BODY = "}";

    /** @var  string */
    protected $name;
    /** @var  string */
    protected $accessible = self::ACCESS_PUBLIC;
    /** @var int */
    protected $indent = 4;
    /** @var array */
    protected $parameters = [];
    /** @var  string */
    protected $body;

    public function __construct()
    {
        $this->phpDoc = new PhpDoc();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAccessible()
    {
        return $this->accessible;
    }

    /**
     * @param string $accessible
     * @throws ElementException
     */
    public function setAccessible($accessible)
    {
        if (!in_array(
            $accessible,
            [
                static::ACCESS_PUBLIC,
                static::ACCESS_PRIVATE,
                static::ACCESS_PROTECTED,
            ]
        )
        ) {
            throw new ElementException (
                'Accessible must be public, private or protected'
            );
        }
        $this->accessible = $accessible;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    public function addAnnotation(AnnotationInterface $annotation)
    {
        $this->phpDoc->addAnnotation($annotation);
    }

    public function addParameter($name, $value = null, $type = null)
    {
        if (is_null($value)) {
            $this->parameters[] = (!is_null($type))
                ? $type . ' $' . $name
                : '$' . $name;
        } else {
            $this->parameters[] = (!is_null($type))
                ? $type . ' $' . $name . ' = ' . $value
                : '$' . $name . ' = ' . $value;
        }
    }

    public function toString()
    {
        if (
            is_null($this->name)
            || is_null($this->body)
        ) {
            throw new ElementException(
                'Method must have name and body'
            );
        }
        $parameters = join(', ', $this->parameters);
        if (is_null($parameters)) {
            $parameters = "";
        }

        $method = $this->phpDoc->toString();
        $method .= sprintf(static::BEGIN_LINE, $this->accessible, $this->name, $parameters);
        $method .= PHP_EOL;
        $method .= static::BEGIN_BODY . PHP_EOL;
        $method .= $this->applyIndent($this->body) . PHP_EOL;
        $method .= static::END_BODY;

        return $this->applyIndent($method);
    }
}