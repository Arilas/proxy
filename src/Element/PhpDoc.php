<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 1:48
 */

namespace Arilas\Proxy\Element;


use Arilas\Proxy\Annotation\AnnotationInterface;

class PhpDoc extends AbstractElement
{
    const BEGIN_TAG = '/**';
    const BEGIN_LINE = ' * ';
    const END_TAG = ' */';

    /** @var AnnotationInterface[] */
    protected $annotations = [];

    public function addAnnotation(AnnotationInterface $annotation)
    {
        $this->annotations[] = $annotation;
    }

    public function toString()
    {
        $doc = static::BEGIN_TAG . PHP_EOL;
        foreach ($this->annotations as $annotation) {
            $doc .= static::BEGIN_LINE;
            $doc .= $annotation->toString();
            $doc .= PHP_EOL;
        }
        $doc .= static::END_TAG . PHP_EOL;

        return $doc;
    }
}