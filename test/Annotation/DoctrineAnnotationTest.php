<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 2:32
 */

namespace Arilas\ProxyTest\Annotation;


use Arilas\Proxy\Annotation\DoctrineAnnotation;

class DoctrineAnnotationTest extends \PHPUnit_Framework_TestCase
{
    protected $typeNeedle = '@ORM\Id()';

    public function testType()
    {
        $annotation = new DoctrineAnnotation();
        $annotation->setType('ORM\Id');

        $this->assertEquals($this->typeNeedle, $annotation->toString());
    }
}
 