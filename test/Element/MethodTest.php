<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 2:56
 */

namespace Arilas\ProxyTest\Element;


use Arilas\Proxy\Element\Method;

class MethodTest extends \PHPUnit_Framework_TestCase
{
    protected $nameNeedle = '    /**
     */
    public function test()
    {
        return $this->test;
    }';

    protected $parametersNeedle = '    /**
     */
    public function test($id, Annotation $anno)
    {
        return $this->test;
    }';

    public function testName()
    {
        $method = new Method();
        $method->setName('test');
        $method->setBody('return $this->test;');

        $this->assertEquals($this->nameNeedle, $method->toString());
    }

    public function testParameters()
    {
        $method = new Method();
        $method->setName('test');
        $method->setBody('return $this->test;');
        $method->addParameter('id');
        $method->addParameter('anno', null, 'Annotation');

        $this->assertEquals($this->parametersNeedle, $method->toString());
    }
}
 