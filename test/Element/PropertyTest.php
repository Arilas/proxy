<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 2:17
 */

namespace Arilas\ProxyTest\Element;


use Arilas\Proxy\Element\Property;

class PropertyTest extends \PHPUnit_Framework_TestCase
{
    protected $nameNeedle = '    /**
     */
    protected $id;';
    protected $accessibleNeedle = '    /**
     */
    public $id;';

    public function testName()
    {
        $property = new Property();
        $property->setName('id');

        $this->assertEquals($this->nameNeedle, $property->toString());
    }

    public function testAccessible()
    {
        $property = new Property();
        $property->setName('id');
        $property->setAccessible(Property::ACCESS_PUBLIC);
        $this->assertEquals($this->accessibleNeedle, $property->toString());
    }
}
 