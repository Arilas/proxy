<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 2:06
 */

namespace Arilas\ProxyTest;

error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);

class Bootstrap
{
    public static function init()
    {
        date_default_timezone_set('UTC');
    }
}

Bootstrap::init();
