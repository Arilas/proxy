<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 3:18
 */

namespace Arilas\ProxyTest\Document;


use Arilas\Proxy\Annotation\DoctrineAnnotation;
use Arilas\Proxy\Document\ClassDocument;
use Arilas\Proxy\Element\Method;
use Arilas\Proxy\Element\Property;

class ClassDocumentTest extends \PHPUnit_Framework_TestCase
{
    protected $nameNeedle = '<?php

namespace Arilas;

/**
 */
class Manager
{
}';

    protected $fullNeedle = '<?php

namespace Arilas;

use Arilas\Proxy\Generator;
use Arilas\Proxy as Proxy;

/**
 * @Proxy\Test(id="name")
 */
class Manager extends Generator implements Proxy\GeneratorInterface
{
    /**
     * @ORM\Id()
     */
    protected $id = null;

    /**
     */
    public function setId($id = null)
    {
        $this->id = $id
    }

}';

    public function testName()
    {
        $document = new ClassDocument();
        $document->setNamespace('Arilas');
        $document->setName('Manager');

        $this->assertEquals($this->nameNeedle, $document->toString());
    }

    public function testFull()
    {
        $document = new ClassDocument();
        $document->setNamespace('Arilas');
        $document->addUseStatement('Arilas\Proxy\Generator');
        $document->addUseStatement('Arilas\Proxy', 'Proxy');

        $annotation = new DoctrineAnnotation();
        $annotation->setType('Proxy\Test');
        $annotation->setValue('id="name"');
        $document->addClassAnnotation($annotation);
        $document->setName('Manager');
        $document->setExtends('Generator');
        $document->addImplement('Proxy\GeneratorInterface');

        $id = new Property();
        $id->setName('id');
        $id->setValue('null');
        $idAnnotation = new DoctrineAnnotation();
        $idAnnotation->setType('ORM\Id');
        $id->addAnnotation($idAnnotation);
        $document->addProperty($id);

        $setter = new Method();
        $setter->setName('setId');
        $setter->addParameter('id', 'null');
        $setter->setBody('$this->id = $id');
        $document->addMethod($setter);

        $this->assertEquals($this->fullNeedle, $document->toString());
    }
}
 