Arilas Proxy Generator
==============

Arilas Proxy Generator is a tool to generate proxy classes on fly.

## Install

### Composer

Install Composer [Composer](http://getcomposer.org):

```bash
$ curl -s https://getcomposer.org/installer | php
```

Add requirements to `composer.json`:

```yaml
{
    "require": {
        "arilas/proxy": "dev-master"
    }
}
```
